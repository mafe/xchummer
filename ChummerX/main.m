//
//  main.m
//  ChummerX
//
//  Created by Marco Feltmann on 21.03.14.
//  Copyright (c) 2014 mfs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
